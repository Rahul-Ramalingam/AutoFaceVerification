# *🧬 Auto Face verification 💻*

---

## Table of Contents

- [Description](#description)
- [Demo](#demo)
- [Further Enhancement](#Further-Enhancement)


---

## Description

This is a face verification web app which can verify your identity.
This web app uses computer vision to verify the identity of the user.When user enters the app he/she is asked to hold their card according to the guide given,
and the card must be clearly visible and user's hand should not block the card.A Mild lighting condition is ideal.The user is Verfied if the face of the user and
the face in the ID card matches. The User must also provide a **valid ID Card**....The valid ID forms are listed below

Acceptable ID forms
- Driver’s Licence
- Passport
- Aadhar card
- PAN card

Technologies Used
- Python
- Deep Learning
- Flask
- HTML,CSS
- Javascript

## Demo
##(video available in testing folder)

<p align="center">
  <img src="testing/Demo.gif"/>
</p>
#### please wait for demo video (2 mins video) to load ####

## Further Enhancement

### Areas Needed Enhancement

#### Card Verification:
The card verification model is built using image similarity concept. This can be further enhanced using a SVM classifier or other ml or dl models

#### Card Detection:
For now , User is asked to place the card in a particular location for card verification. This can be automated by using a state of art object detection models
          like yolo, resnet etc..The models can be trained using transfer learning and can be used for detection
